@echo off

set title=Minecraft Bungee Server
set jar=waterfall_minecraft_server.jar
set MINRAM=1G
set MAXRAM=1G

title %title%
cls
echo ===============================
echo !!                           !!
echo !!   MINECRAFT BUNGEE ATOM   !!
echo !!          SERVER           !!
echo !!        (Waterfall)        !!
echo !!                           !!
echo ===============================
echo.
echo.
echo Starting Server !
echo.
echo.
java -Xms%MINRAM% -Xmx%MAXRAM% -jar %jar% nogui
echo.
echo.
color 0c
echo Server Stopped.

pause>nul